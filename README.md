# Annotator Helper

Retrieve and classify abstracts from Pubmed. 

---

## Table of contents

- [Motivation](#motivation)
- [Data sources](#data-sources)
- [Current version](#current-version)
- [Installation](#installation)
    - [Pre-requisites](#pre-requisites)
    - [Getting the code](#getting-the-code)
    - [Install for development](#install-for-development)
    - [Install for production](#install-for-production)
- [Usage](#usage)
    - [Command line interface](#command-line-interface)
- [Status](#status)
- [License](#license)
- [Contributors](#contributors)
- [Acknowledgments](#acknowledgments)

---

## Motivation

The goal of this project is to retrieve and classify abstracts from scientific publications in order to help annotation by database curators.  
The project is currently aimed at the Intrinsically Disordered Proteins research community and focused on curation of Short Linear Motifs literature.

Publications are retrieved from Pubmed.  
Relevant features are extracted only from the abstracts.  

Classification is performed by a choice of machine learning algorithms.  

The output is a list of high-scoring terms by article and their classification.

## Data sources

At the moment we are recovering data from PubMed articles.

We are currently focused in helping the annotation of Short Linear Motifs and protein intrinsic disorder literature.

Supervised training of machine learning classifiers is based on a curated database of ~2,000 articles on protein intrinsic disorder.

We are also integrating this resource with idp-fun tools:
- NormanDownloaders (data retrieval)
- Ontologies (not yet defined)

## Current version

The current version of this software is 0.1.1.  
We use [Semantic Versioning](https://semver.org/).

---

## Installation

### Pre-requisites
- A Linux system, or a Windows system with Linux shell emulation (e.g. with [Git BASH](https://gitforwindows.org/))
- Python 2.7

### Getting the code

Navigate to the destination folder on your computer, then download the code from the GiLab repository.  
A new folder called 'annotator-helper' will be created.

```bash
    # Navigate to the directory of choice. As an example we use a Linux system home directory.  
    $ cd ~
    # Clone the repository from the GitLab group IDPfun. You may need credentials to get the code.  
    $ git clone git@gitlab.com:idpfun/annotator-helper.git
```

### Install for development

Clone the repo as shown in [Getting the code](#Getting-the-code).  
Then create a virtual environment, activate it and install dependencies.

```bash
    # Create a virtual environment named 'virtualenv'
    $ cd annotator-helper
    $ python -m virtualenv virtualenv
    # Activate the virtual enviroment
    # Windows + git bash:
    source virtualenv/Scripts/activate
    # Linux:
    source virtualenv/bin/activate
    # Install third-party dependencies
    (virtualenv) $ pip install -r requirements.txt
    # Install our package 'downloaders'
    (virtualenv) $ pip install git+https://gitlab.com/idpfun/norman-downloaders.git
```

### Install for production 

For the moment, follow the guidelines for [development installation](#for-development).  

We also plan to have a distributable package and a Docker container in the near future.

---

## Usage

You can run it as a command line program.  

Guidelines on basic functionality are provided below. For further details on how to run the software, 
including an extended description of the project workflow and run parameters, 
please see the project's [Wiki page](https://gitlab.com/NPalopoli/annotator-helper/wikis/home).

### Command line interface

#### Training

The first step should be to train a classifier on known article data:

```bash
    # Train a classifier on data from a selection of relevant literature
    $ python articleClassifier.py --train_abstract_classifier True --classifier_name elm --training_data ./article_class.json
```

####

Once a model is available it can be used to classify the desired abstract(s):

```bash
    # Run a classifier on a comma-separated list of Pubmed IDs
    $ python articleClassifier.py  --classify_abstract True --classifier_name elm --pmid 27939943,27453045
```

We have included an example file with a list of 200 PMIDs that can be used to try classification:

```bash
    # Run a classifier on an example list of Pubmed IDs from file
    $ python articleClassifier.py  --classify_abstract True --classifier_name elm --pmid_file test_pmids.tdt
```


####

We also provide a built-in benchmarking option that uses a custom dataset of Short Linear Motifs-related publications:

```bash
    # Run benchmark on available models
    $ python articleClassifier.py --run_benchmarking True --training_data ./article_class.json
```
---

## Status

This project should be functional, but please open an issue or contact the [authors](#contributors) if you find any bugs.

To-do list:

- Provide help message  
- Write exteded documentation  
- Add further tests
- Make package  
- Make Docker container

## License

This software is available for download and use under the GNU General Public License version 3 (GNU GPL v3). It allows the users (individuals, organizations, companies) to run, study, share (copy), and modify the software. However, it requires that when distributing derived works, the source code of the work must be made available under the same license. For more details please see [LICENSE](LICENSE.md) or check the full [GPL GNU version 3 license](https://www.gnu.org/licenses/gpl.txt).

---

## Contributors

| Name             | Email                 | Role           |
| ---------------- | --------------------- | -------------- |
| Norman Davey     | normandavey@gmail.com | Lead Developer |
| Nicolas Palopoli | nicopalo@gmail.com    | Developer      |

## Acknowledgments

This software was produced as part of the 
**IDPfun, Research and Innovation Staff Exchange project** 
([IDPfun](http://idpfun.eu/)) 
funded by **Marie Skłodowska-Curie - Horizon 2020 programme**
of the **European Commission**.
