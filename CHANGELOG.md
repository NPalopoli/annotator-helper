# Changelog

This project adheres to [Semantic Versioning]()

## Unreleased

- Add details on argument options in [articleClassifier.py](./textmining/articleClassifier.py).
- Add help message option.

## 0.1.1 - 2019-02-21

### Added

- Added [articleClassifier.py](./textmining/articleClassifier.py) to repository. This is the main script of this project.
- Added benchmarking dataset as [article_class.json](./textmining/article_class.json) and [article_class.tsv](./textmining/article_class.tsv).
- Added list of Pubmed IDs for trying classification as [test_pmids.txt](./textmining/test_pmids.tdt).
- Added configuration file [config.ini](./config.ini).
- Added configuration parsers [config_reader.py](./config_reader.py) and [option_reader.py](./option_reader.py).
- Added [LICENSE.md](LICENSE.md) and [CHANGELOG.md](CHANGELOG.md) to repository.
- Added [requirements.txt](requirements.txt) to track package dependencies.

### Changed

- Updated [README.md](README.md) with further documentation including usage instructions.
- Updated list of files and directories not to be tracked in [.gitignore](./.gitignore).

## 0.1.0 - 2019-02-12

### Added

- Creation of private repository. Code yet to be added. Only [README.md](README.md) and [.gitignore](.gitignore) available. 