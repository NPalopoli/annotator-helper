#!/usr/bin/python2.7

# option_reader - Parser for commandline options
# By XX
# Shared under XX

"""
Module:       option_reader
Description:  Parser for commandline options
Version:      -
Last Edit:    2018-12-11
Functions:
    isfloat
	load_commandline_options
Dependencies:
	General: os,inspect,optparse,pprint
"""

################################################################################
## SECTION I: GENERAL SETUP & PROGRAM DETAILS                                 ##
################################################################################

# General imports
import os,inspect,optparse,pprint

################################################################################
## SECTION II: FUNCTIONS                                                      ##
################################################################################

##----------------------------------------------------------------------------##
## isfloat function - check if value is float                                 ##
##----------------------------------------------------------------------------##
def isfloat(value):
	'''
	Returns True is value is float
	'''
	try:
		float(value)
		return True
	except ValueError:
		return False

##----------------------------------------------------------------------------##
## load_commandline_options function - Parse options from commandline         ##
##----------------------------------------------------------------------------##
def load_commandline_options(options,options_help):
	'''
	Read and parse options from commandline arguments
	'''	
	commandline_options = {}
	# Create object of class OptionParser 	
	op = optparse.OptionParser()  # DEPRECATED (v2.7+)
	
	# Iterate over passed options
	for option in options:
		# Set help text
		help_text = "--Not set--"
		if option in options_help:
			help_text = options_help[option]
		# Store options
		op.add_option("--" + option,
			action="store", 
			dest=option,
			default=options[option],
			help=help_text)
	
	# Set attributes to options
	opts, args = op.parse_args()
	
	# Iterate over options dict
	for option in vars(opts):
		try:
			# Iterate over name,value pairs in option
			if getattr(opts, option) == "True" or getattr(opts, option) == True:
				commandline_options[option] = True
			elif getattr(opts, option) == "False" or getattr(opts, option) == False:
				commandline_options[option] = False
			elif isfloat(getattr(opts, option)):
				if str(getattr(opts, option)).count(".") > 0:
					commandline_options[option] = float(getattr(opts, option))
				else:
					commandline_options[option] = int(getattr(opts, option))
			else:			
				commandline_options[option] = getattr(opts, option)
		except:
			raise
	
	# Return parsed options as dict
	return commandline_options
	
################################################################################
## END OF SECTION II                                                          ##
################################################################################