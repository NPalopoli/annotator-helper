#!/usr/bin/python2.7

# articleClassifier - Classify articles using text-mining and machine learning
# By Nicolas Palopoli and Norman Davey 
# Shared under license

"""
Module:       articleClassifier
Description:  Classify articles using text-mining and machine learning
Version:      0.1.1
Last Edit:    2019-02-21
Methods:
    XX
Objects:
    classifier = 
Commandline:
    XX
Dependencies:
	General: 	os,sys,time,json,random,itertools,pickle,urllib,urllib2,copy,
	         	optparse,warnings,inspect,stat,pprint,gzip,ntlk,sklearn,
			 			ConfigParser,numpy,ftplib,xml
	Custom: 
"""


__description__ = """
articleClassifier.py - Classify articles using text-mining and machine learning

The script has five major parts:

(1) Data preparation
(2) TF-IDF document scoring
(3) Machine learning of TF-IDF document scores for classifier model construction
(4) Classification using the model
(5) Benchmarking

(1) Data preparation

(2) TF-IDF document scoring
Stack overflow description of how the IDF works - 'The IDF part of TF-IDF gives less weight 
to a word if it occurs in a large fraction of the documents in your corpus. However, this 
doesn't necessarily mean that the word is  unimportant for distinguishing your two classes. 
A word which is common in your corpus,  but which also occurs substantially more often in 
one class than the other, could very well be quite valuable in distinguishing the classes. 
This can especially be true if your set is not balanced between the two classes.'

(3) Machine learning of TF-IDF document scores for classifier model construction
(4) Classification using the model
(5) Benchmarking

"""

__example__ = """
#############################
### Training a classifier ###
#############################
python articleClassifier.py --train_abstract_classifier True --classifier_name elm --training_data ./article_class.json

[Required]
  --train_abstract_classifier=TRAIN_ABSTRACT_CLASSIFIER
                        Whether to run the training.
  --classifier_name=CLASSIFIER_NAME
                        Name of the model to use to classify papers.
 --training_data=TRAINING_DATA
                        Data to train classifier.                        
[Optional]
  --training_data_type=TRAINING_DATA_TYPE
												File type of training dataset ["json","tdt"]
  --strip_non_nouns=STRIP_NON_NOUNS
                        Remove nouns from abstract before training model.
  --classifier_method=CLASSIFIER_METHOD
                        Machine learning classifier method [kNN|MultinomialNB_
                        Naive_Bayes|RBF_SVC|passive_aggressive|linearSVC_l2|li
                        nearSVC_feature_selection_l1|linearSVC_feature_selecti
                        on_l2|SGDClassifier_l1|SGDClassifier_l2|BernoulliNB_Na
                        ive_Bayes|random_forest|ridge_classifier|perceptron|ne
                        arestCentroid|elastic_net|linearSVC_l1]
  --classifier_use_protein_annotation=[TRUE,FALSE]
												Inject PubMed protein_annotation into the abstracts [gene names, protein names]
  --classifier_use_mesh_terms=[TRUE,FALSE]
												Inject mesh terms into the abstracts
                        
#######################
### Screening PMIDs ###
#######################
python articleClassifier.py  --classify_abstract True --classifier_name elm_test --pmid 27939943,27453045

[Required]
  --classify_abstract
  | --pmid=PMID
												Pubmed id of article for classification.
  | OR
  | --pmid_file=PMID_FILE
                        File with pubmed ids of article for classification.
                        One per line.
  --classifier_name=CLASSIFIER_NAME
                        Name of the model to use to classify papers.

[Optional]
  --distance_cutoff=DISTANCE_CUTOFF
                        Cut-off for distance to the hyperplane. Positive is a
                        good prediction.
  --show_keywords=SHOW_KEYWORDS
                        Show the most important keywords for each class.
  --classify_pubmed=CLASSIFY_PUBMED
                        Classify pubmed.
  --output_path=OUTPUT_PATH
  --output_type=OUTPUT_TYPE
  --show_training_articles=SHOW_TRAINING_ARTICLES
  --make_output=MAKE_OUTPUT
  --significance_cutoff=SIGNIFICANCE_CUTOFF
  --classification_type=CLASSIFICATION_TYPE
                        [top|significant|distance]
  --classifier_use_protein_annotation=[TRUE,FALSE]
												Inject PubMed protein_annotation into the abstracts [gene names, protein names]
  --classifier_use_mesh_terms=[TRUE,FALSE]
												Inject mesh terms into the abstracts
                        
############################
### Running benchmarking ### 
############################

python articleClassifier.py --run_benchmarking True --training_data ./article_class.json

[Required]
  --run_benchmarking
  --training_data=TRAINING_DATA
                        Data to train classifier.    
[Optional]
  --xfold=XFOLD         Number of partitions in the cross validation.
	--classifier_use_protein_annotation=TRUE/FALSE
												Inject PubMed protein_annotation into the abstracts [gene names, protein names]
  --classifier_use_mesh_terms=TRUE/FALSE
												Inject mesh terms into the abstracts
  
"""


################################################################################
## SECTION I: GENERAL SETUP & PROGRAM DETAILS                                 ##
################################################################################

# General imports
import os,sys,time,json,random,itertools,pickle,urllib2,copy,optparse,warnings,inspect,stat,pprint,gzip,operator,re,string
# General imports: configuration file parser
from ConfigParser import SafeConfigParser
# General imports: file download and parsing
from ftplib import FTP
from urllib import urlopen
from xml.etree.ElementTree import iterparse
from xml.etree import cElementTree as elementtree

# General imports: math
import numpy as np
warnings.filterwarnings("ignore")  # suppress numpy warnings
import scipy as sp
# General imports: text mining
import nltk
# General imports: machine learning
from sklearn import preprocessing
from sklearn.utils.multiclass import is_multilabel
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_selection import SelectKBest, chi2, SelectFromModel
from sklearn.linear_model import RidgeClassifier
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn import metrics

# Custom imports: downloaders
import downloaders.pmidDownloader as pmidDownloader
# Custom imports: configuration
sys.path.append("../")
import config_reader
import option_reader

################################################################################
## END OF SECTION I                                                           ##
################################################################################


################################################################################
## SECTION II: CLASSES                                                        ##
################################################################################

##----------------------------------------------------------------------------##
## articleClassifier class - data from articles and its classification        ##
##----------------------------------------------------------------------------##

class articleClassifier():
	def __init__(self):

  	# Defaults for command line arguments
		self.options = {
		"xfold":5,
		"strip_non_nouns":False,
		"show_keywords":False,
		"run_benchmarking":False,
		"train_abstract_classifier":False,
		"classify_abstract":False,
		"classify_candidates":False,
		"classify_pubmed":False,
		"pmid":'',
		"pmid_file":'',
		"training_data":'',
		"training_data_type":'tdt',
		"significance_cutoff":0.01,
		'classification_type':"significant",
		"distance_cutoff":0,
		"show_training_articles":True,
		"show_classifiers":False,
		"output_path":"",
		"output_type":'tdt',
		"make_output":False,
		"show_output":True,
		"classifier_name":'',
		"classifier":'',
		"classifer_method":'SGDClassifier_l2',
		"classifier_use_mesh_terms":False,
		"classifier_use_protein_annotation":False
		}

		# Load external values
		self.options.update(config_reader.load_configeration_options())

		# Benchmark methods available
		self.benchmark_methods = [
		"Ridge Classifier",
		"Perceptron",
		"Passive-Aggressive",
		"kNN",
		"Random forest",
		"LinearSVC %s penalty l1",
		"SGDClassifier %s penalty l1",
		"LinearSVC %s penalty l2",
		"SGDClassifier %s penalty l2",
		"RBF SVC %s penalty",
		"Elastic-Net penalty",
		"NearestCentroid (aka Rocchio classifier)",
		"MultinomialNB Naive Bayes",
		"BernoulliNB Naive Bayes",
		"LinearSVC with L1-based feature selection",
		"LinearSVC with L2-based feature selection"
		]

		# Benchmark methods names
		self.benchmark_methods_options = {
		"ridge_classifier":"Ridge Classifier",
		"perceptron":"Perceptron",
		"passive_aggressive":"Passive-Aggressive",
		"kNN":"kNN",
		"random_forest":"Random forest",
		"linearSVC_l1":"LinearSVC %s penalty l1",
		"SGDClassifier_l1":"SGDClassifier %s penalty l1",
		"linearSVC_l2":"LinearSVC %s penalty l2",
		"SGDClassifier_l2":"SGDClassifier %s penalty l2",
		"RBF_SVC":"RBF SVC %s penalty",
		"elastic_net":"Elastic-Net penalty",
		"nearestCentroid":"NearestCentroid (aka Rocchio classifier)",
		"MultinomialNB_Naive_Bayes":"MultinomialNB Naive Bayes",
		"BernoulliNB_Naive_Bayes":"BernoulliNB Naive Bayes",
		"linearSVC_feature_selection_l1":"LinearSVC with L1-based feature selection",
		"linearSVC_feature_selection_l2":"LinearSVC with L2-based feature selection"
		}
		
		# Command line arguments definitions
		self.options_help = {
		"xfold":"Number of partitions in the cross validation.",
		"strip_non_nouns":"Remove nouns from abstract before training model.",
		"show_keywords":"Show the most important keywords for each class.",
		"run_benchmarking":"Whether to run the benchmarking.",
		"train_abstract_classifier":"Whether to run the training.",
		"classify_abstract":"Classify an abstract.",
		"classify_candidates":"Classify candidates.",
		"classify_pubmed":"Classify pubmed.",
		"pmid":"Pubmed id of article for classification.",
		"pmid_file":"File with pubmed ids of article for classification. One per line.",
		"training_data":"Data to train classifier.",
		"training_data_type":"tdt",
		"significance_cutoff":"",
		'classification_type':"[top|significant|distance]",
		"distance_cutoff":"Cut-off for distance to the hyperplane. Positive is a good prediction. ",
		"show_training_articles":"",
		"output_path":"",
		"output_type":"[json|tdt]",
		"make_output":"",
		"show_output":"",
		"classifier_name":'Name of the model to use to classify papers',
		"classifier_method":'Machine learning classifier method [' + "|".join(self.benchmark_methods_options.keys()) + ']',
		"classifier_use_mesh_terms":"Inject mesh terms into the abstracts",
		"classifier_use_protein_annotation":"Inject PubMed protein_annotation into the abstracts [gene names, protein names]",
		}

    # Initialize empty arguments
		self.benchmark_results = {}
		for benchmark_method in self.benchmark_methods:
			self.benchmark_results[benchmark_method] = []
		self.abstracts = []
		self.specificity_classes = []
		self.article_classes = []
		self.model_data = {}
		self.article_data = {}
		self.specificity_class_data = {}
		self.article_class_data = {}
		
		# Activate to connect to local database
		grab_auth = False
		if grab_auth:
			url = self.options["url_root"] + "/resource/login?login=Norman&password=Norman" 
			login_response = urllib2.urlopen(url).read()
			login_response = json.loads(login_response)
			# Copy User-Agent headers value as object attribute
			self.headers = login_response["headers"]

	##------------------------------------------------------------------------##
	## setup_paths method - Setup working directories
	##------------------------------------------------------------------------##	
	def setup_paths(self):	
		'''
		Check working directories exist or create if needed
		'''
		# Add a pubmed downloader object
		self.pmidDownloaderObj = pmidDownloader.pmidDownloader()
		self.pmidDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])
		
		self.options["pubmed_path"] = os.path.abspath(os.path.join(self.options["data_path"],'pubmed'))
		self.options["pubmed_bulk_path"]  = os.path.abspath(os.path.join(self.options["data_path"],'pubmed',"bulk"))
		self.options["nltk_path"] = os.path.abspath(os.path.join(self.options["data_path"],"nltk"))
		self.options["models_path"] = os.path.abspath(os.path.join(self.options["data_path"],'text_mining_models'))
		
		self.options["model_path"] =  os.path.abspath(os.path.join(self.options["models_path"], self.options["classifier_name"] + "." + self.benchmark_methods_options[self.options["classifer_method"]].replace(" ","_").lower() + ".model.pickle"))
		self.options["model_results_path"] = os.path.abspath(os.path.join(self.options["models_path"], self.options["classifier_name"] + "_" + self.options["classifer_method"]))
		
		for path in ['pubmed_path','pubmed_bulk_path','nltk_path','models_path',"model_results_path"]:
			if not os.path.exists(self.options[path]):
				print "Making",path,self.options[path]
				os.mkdir(self.options[path])
			
	##------------------------------------------------------------------------##
	## getReference method - Retrieve and parse articles
	##------------------------------------------------------------------------##	
	def getReference(self,pmid):
		'''
		Retrieve articles and parse details and annotations
		'''
		# Retrieve references
		pubmed_data = self.pmidDownloaderObj.parsePMID(pmid)
		if self.options["classifier_use_protein_annotation"]:
			# Parse SciLite annotation
			pubmed_data['scilite_tags'] = []
			try:
				pubmed_data['scilite'] = self.pmidDownloaderObj.parseAnnotationByPMID(pmid,True)['data']
				for accession in pubmed_data['scilite']:
					pubmed_data['scilite_tags'] += pubmed_data['scilite'][accession]['gene_names'] + [pubmed_data['scilite'][accession]['protein_name']] + pubmed_data['scilite'][accession]['protein_names']
			except:
				pass
			# Parse PDB annotation
			pubmed_data['pdb_annotation_tags'] = []
			try:
				for accession in pubmed_data['data']['proteins']:
					pubmed_data['pdb_annotation_tags'] += pubmed_data['data']['proteins'][accession]['gene_names'] + [pubmed_data['data']['proteins'][accession]['protein_name']] + pubmed_data['data']['proteins'][accession]['protein_names']
			except:
				pass
		# Return dictionary with parsed annotations
		return pubmed_data

	##------------------------------------------------------------------------##
	## check_custom_directory method - Ensure path exists
	##------------------------------------------------------------------------##
	def check_custom_directory(self,target_path):
		'''
		Ensure output path exists
		'''
		if not os.path.exists(self.options[target_path]):
			os.mkdir(self.options[target_path])
	
	##------------------------------------------------------------------------##
	## getTrainingDataLocal method - Convert instances file to object
	##------------------------------------------------------------------------##		
	def getTrainingDataLocal(self):
		'''
		Read specificity classes file from local folder in JSON and convert into Python object
		'''
		if self.options["training_data_type"] == "json":
			# Parse specificity classes file in JSON if present		
			with open(self.options["training_data"]) as data_file:    
				data = json.load(data_file)  # deserialize file into object
		if self.options["training_data_type"] == "tdt":
			# Parse specificity classes file in TDT if present
			data = {}
			for line in open(self.options["training_data"]).read().strip().split("\n"):
				line_bits = line.split()
				if line_bits[0] not in data:
					data[line_bits[0]] = []
				data[line_bits[0]].append(line_bits[1])
		#response = self.getReference(self.options["pmid"])
		references = {}
		# Iterate over classes and Pubmed IDs
		for class_name in data:
			for pmid in data[class_name]:
				if pmid not in references:
					# Get article data from source if needed
					response = self.getReference(pmid)
					if response[u'status'] == u'Error':
						print "Error retrieving article",pmid
						continue
					references[pmid] = response['data']					
					for data_type in response:
						if data_type != 'data':
							references[pmid][data_type] = response[data_type]
					references[pmid]["class"] = []
				references[pmid]["class"].append(class_name)
		# Store classes as object attribute
		self.article_class_name = data.keys()
		# Return dictionary of article data
		return references
		
	##########################################################################################
	##########################################################################################
	##########################################################################################
	#PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBMED PUBME
	##########################################################################################
	##########################################################################################
	##########################################################################################
	
	##------------------------------------------------------------------------##
	## grabPubmedXml method - Retrieve Pubmed in XML format
	##------------------------------------------------------------------------##
	def grabPubmedXml(self):
		'''
		Retrieve in bulk from Pubmed in XML format
		'''
		# Connect to NCBI FTP baseline repository
		ftp = FTP('ftp.ncbi.nlm.nih.gov')
		ftp.login()   
		ftp.cwd('/pubmed/baseline/')  
		# Setup local directories
		out_path = os.path.join(self.options["pubmed_path"],"baseline_dir_list.txt")
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
				os.remove(out_path)
		if not os.path.exists(out_path):  
			open(out_path,"w").write("\n".join(ftp.nlst()))
		dir_list = open(out_path).read()
		if not os.path.exists(os.path.join(self.options["pubmed_path"],"baseline_dir_list.txt")):      
			open(os.path.join(self.options["pubmed_path"],"baseline_dir_list.txt"),"w").write( "\n".join(ftp.nlst()))
		dir_list = open(os.path.join(self.options["pubmed_path"],"baseline_dir_list.txt")).read()
		# Retrieve binary files
		for filename in dir_list.split("\n"):
			if filename.split(".")[-1] == "gz"  and not os.path.exists(os.path.join(self.options["pubmed_bulk_path"], filename) ):
				local_filename = os.path.join(self.options["pubmed_bulk_path"], filename)
				file = open(local_filename, 'wb')
				print ftp.retrbinary('RETR '+ filename, file.write)
		# Setup daily updates directories
		ftp.cwd('/pubmed/updatefiles/')  
		out_path = os.path.join(self.options["pubmed_path"],"updatefiles_dir_list.txt")
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
				os.remove(out_path)
		if not os.path.exists(out_path):  
			open(out_path,"w").write( "\n".join(ftp.nlst()))
		dir_list = open(os.path.join(self.options["pubmed_path"],"updatefiles_dir_list.txt")).read()
		# Retrieve binary files
		for filename in dir_list.split("\n"):
			if  filename.split(".")[-1] == "gz" and not os.path.exists(os.path.join(self.options["pubmed_bulk_path"], filename) ):
				local_filename =os.path.join(self.options["pubmed_bulk_path"], filename)
				file = open(local_filename, 'wb')
				print ftp.retrbinary('RETR '+ filename, file.write)
		# Disconnect from FTP
		ftp.quit()

	##------------------------------------------------------------------------##
	## parsePubmedXml method - Parse Pubmed in XML format
	##------------------------------------------------------------------------##
	def parsePubmedXml(self,xml_file):
		'''
		Parse Pubmed records in XML format
		'''
		articles_details = {}
		# Iterate over XML
		for event, elem in iterparse(xml_file):
			# Retrieve citation element
			if elem.tag == "MedlineCitation":
				# Retrieve abstracts from articles
				if elem.find("Article").find("Abstract") != None:
					PMID = elem.findtext("PMID")
					article_data = {}
					# Retrieve and clean abstract and title
					for tag_name in ["AbstractText","ArticleTitle"]:
						article_data[tag_name] = ""
						for tag_elem in elem.iter(tag_name):
							try:
								text = elementtree.tostring(tag_elem, encoding='utf-8', method='xml')
								text = re.sub('<[^<]+?>', '', text)
								text = text.strip().replace("\n"," ")
								if tag_name not in article_data:
									article_data[tag_name] = ""
								article_data[tag_name] += filter(lambda x: x in string.printable, text)
							except:
								raise
					# Link PMID with abstract and title in dict
					if article_data['AbstractText'] != None:
						articles_details[PMID]  = {
						"ArticleTitle":article_data['ArticleTitle'],
						"AbstractText":article_data['AbstractText'] 
						}
				elem.clear()
		# Return parsed articles
		return articles_details
		
	##------------------------------------------------------------------------##
	## classifyPubmed method - Classify Pubmed in XML format
	##------------------------------------------------------------------------##
	def classifyPubmed(self):
		'''
		Classify Pubmed records stored in XML format in bulk
		'''
		counter = 0
		# List directories with Pubmed records
		classify_directories = os.listdir(os.path.join(self.options["pubmed_bulk_path"]))
		classify_directories.sort()
		classify_directories.reverse()
		# Iterate over (zipped) XML files of Pubmed record
		for xml_file in classify_directories:
			try:
				if xml_file.split(".")[-1] == "gz":
					# Setup file names
					tdt_file = os.path.join(self.options["pubmed_bulk_path"],xml_file)[:-3] + ".tdt"
					results_tdt_file = os.path.join(self.options["pubmed_bulk_path"],xml_file)[:-3] + ".results.tdt"
					xml_file = os.path.join(self.options["pubmed_bulk_path"],xml_file)
					print "Parsing",xml_file
					# Parse XML file if not present
					if not os.path.exists(tdt_file) or os.path.exists(tdt_file):
						# Unzip file
						inF = gzip.open(xml_file, 'rb')
						s = inF.read()
						inF.close()
						uncompressed_path = os.path.join(os.path.join(self.options["pubmed_bulk_path"], xml_file[:-3]))
						open(uncompressed_path, 'w').write(s)
						# Parse XML file
						articles = self.parsePubmedXml(uncompressed_path)				
						tdt_str = ""
						results_tdt_str = ""
						# Iterate over articles in XML file
						for pmid in articles:
							counter += 1
							self.options["pmid"] = pmid
							self.options["title"] = articles[pmid]["ArticleTitle"]
							self.options["abstract"] = articles[pmid]["AbstractText"]
							# Join PMID, title and abstract as Unicode string
							tdt_str += "\t".join([self.options["pmid"],''.join(i for i in self.options["title"] if ord(i)<128), ''.join(i for i in self.options["abstract"] if ord(i)<128) ]) + "\n"
							# Classify article and link PMID to output metrics 
							classification = self.run_classifier()
							top_hit = classification[pmid]['top'].keys()[0]
							bonferroni_corrected_probability = classification[pmid]['scores'][top_hit]['bonferroni_corrected_probability']
							probability = classification[pmid]['scores'][top_hit]['probability']
							distance  = classification[pmid]['scores'][top_hit]['distance']
							training_article = classification[pmid]['training_article']
							# Print table of output metrics
							if distance > 0:
								print counter,"\t",
								tmp_results_tdt_str = str(self.options["pmid"]) + "\t"
								tmp_results_tdt_str += str(top_hit) + "\t"
								tmp_results_tdt_str += str(bonferroni_corrected_probability) + "\t"
								tmp_results_tdt_str += str(probability) + "\t"
								tmp_results_tdt_str += str(distance) + "\t"
								tmp_results_tdt_str += str(training_article) + "\t"
								tmp_results_tdt_str += str("|".join(classification[pmid]['top'][top_hit].keys())) + "\t"
								tmp_results_tdt_str += str(''.join(i for i in self.options["title"] if ord(i)<128)) + "\t"
								tmp_results_tdt_str += str(''.join(i for i in self.options["abstract"] if ord(i)<128)) + "\n"
								print tmp_results_tdt_str
								results_tdt_str += tmp_results_tdt_str
								# Save classification to JSON file
								with open(os.path.join(self.options["model_results_path"],self.options["pmid"] + ".json"), 'w') as outfile:
									json.dump(classification, outfile)
						# Save article details to TDT file
						del articles
						print "Writing tdt",tdt_file
						open(tdt_file,"w").write(tdt_str)
						os.remove(uncompressed_path)
					print xml_file,counter
			# Raise exception if failed
			except Exception, e:
				print "Failed to parse",xml_file,e
				raise
	
	
	##########################################################################################
	##########################################################################################
	##########################################################################################
	#DATASETS DATASETS DATASETS DATASETS DATASETS DATASETS DATASETS DATASETS DATASETS DATASETS
	##########################################################################################
	##########################################################################################

	##------------------------------------------------------------------------##
	## nltk_download method - Download NLTK package if needed
	##------------------------------------------------------------------------##	
	def nltk_download(self,package_path,package_name):
		'''
		Download NLTK package if needed
		'''
		# Check if package is already available or download otherwise
		try:
			nltk.data.path.append(self.options["nltk_path"])  # append custom nltk path
			nltk.data.find(os.path.join(package_path,package_name))  # check if package is available
		except LookupError:
			nltk.download(package_name, download_dir=self.options["nltk_path"])  # download package
		return()
	
	##------------------------------------------------------------------------##
	## load_papers_pubmed method - Retrieve papers downloaded from Pubmed
	##------------------------------------------------------------------------##	
	def load_papers_pubmed(self):
		'''
		Retrieve papers downloaded from Pubmed
		'''
		
		# Read references file from local file into object as dict
		references = self.getTrainingDataLocal()
		# Create string for unnannotated entries
		unannotated_tag = "-1:candidate"
		# Iterate over all reference IDs in references dict
		for article in references.keys():
			# Read PMID into object attribute
			pmid = references[article][u'pmid']
			self.article_data[pmid] = references[article]
			# Link PMID to article class ID as object attribute
			if self.article_data[pmid]["class"] == []:
				self.article_data[pmid]["class"] = unannotated_tag  # use unannotated tag if absent
			# Make training text by linking title, abstract, annotations and MeSH terms into string as object attribute
			abstract_text = self.article_data[pmid]['title'] + " " + self.article_data[pmid]["abstract_text"].replace('\n', ' ').replace('.', ' ') 
			if self.options["classifier_use_protein_annotation"]:
				if "pdb_annotation_tags" in self.article_data[pmid]:
					abstract_text +=  " ".join(self.article_data[pmid]["pdb_annotation_tags"])  + " " 	
				if "scilite_tags" in self.article_data[pmid]:
					abstract_text +=  " ".join(self.article_data[pmid]["scilite_tags"])
			if self.options["classifier_use_mesh_terms"]:
				abstract_text +=  " ".join( self.article_data[pmid]["mesh_terms"]) + " " 			
			training_text = abstract_text
			# Download Punkt Tokenizer Model to parse English if needed
			self.nltk_download('tokenizers','punkt')
			# Split training text into words, spliting off punctuation other than periods
			tokens_unique = nltk.word_tokenize(training_text.lower())
			# Convert splitted text into nltk.Text object
			tokens_text = nltk.Text(tokens_unique)
			# Download Averaged Perceptron Tagger if needed
			self.nltk_download('taggers','averaged_perceptron_tagger')
			# Classify words into lexical categories (POS-tagging) as tuples
			tagged = nltk.pos_tag(tokens_unique)
			# Create empty list of nouns
			nouns = []
			# Iterate over tagged words tuples
			for token in tagged:
				# todo: strip numbers away
				token_text = token[0]  # word
				token_type = token[1]  # tag
				# Avoid non-nouns if requested
				if self.options["strip_non_nouns"]:
					# Add nouns to nouns list
					if token_type in ["NN","NNS","NNP","NNPS"]:  # (single, plural) common, proper nouns
						nouns.append(token_text.lower().strip())
				# Add all words to nouns list
				else:
					nouns.append(token_text.lower().strip())
			# Join words and link to PMID as object attribute
			self.article_data[pmid]["training_text"] = " ".join(nouns).replace("(","").replace(")","")
		# Make list of PMIDs
		pmids = self.article_class_data.keys()
		# Shuffle PMIDs list in place
		random.shuffle(pmids)
		# Iterate over PMIDs
		for pmid in self.article_data:
			# Add unnannotated entries to lists of loaded abstracts and article classes as object attributes
			if self.article_data[pmid]["class"] != unannotated_tag:
				for article_class in self.article_data[pmid]["class"]:
					self.abstracts.append(self.article_data[pmid]["training_text"] )
					self.article_classes.append(article_class)
		# Print number of loaded abstracts and article classes
		print "Loaded papers:",len(self.abstracts)
		print "Loaded classes:",len(list(set(self.article_classes)))

	##------------------------------------------------------------------------##
	## make_vectorizer method - Create vectorizer to generate matrix of word occurrences
	##------------------------------------------------------------------------##		
	def make_vectorizer(self):
		'''
		Create vectorizer to convert text document to matrix of word occurrences
		'''
		# Convert text to matrix of TF-IDF (term-frequency times inverse-document-frequency) word ocurrences
		# Tfidf is high when term is frequent in document but not in collection of documents
		if self.options["vectorizer_type"] == "TfidfVectorizer":
			# Sublinear tf scaling (1 + log(tf)); filter out common words in English; extract unigrams only; tokens of 2+ alphanumeric
			self.vectorizer = TfidfVectorizer(sublinear_tf=True, stop_words='english',ngram_range=(1,1),token_pattern='[0-9A-Za-z-]{2,}')
			print "Created Tfidf vectorizer"
		# Convert text to matrix applying hashing function to term frequencies and use hash values as word indices
		if self.options["vectorizer_type"] == "HashingVectorizer":
			# Avoid negative word counts; filter out common words in English; allow 10000 columns (words) in matrix
			self.vectorizer = HashingVectorizer(stop_words='english', non_negative=True, n_features=10000)
			print "Created Hashing vectorizer"
	
	##------------------------------------------------------------------------##
	## setup_data function - Load classes and papers
	##------------------------------------------------------------------------##
	def setup_data(self):
		'''
		Load papers classes and papers
		'''
		self.getTrainingDataLocal()
		# If 3+ classes available, load papers and create vectorizer
		if len(self.article_class_name) < 3:
			print "The article classifier needs at least 3 classes to work"
			pprint.pprint(self.article_class_name)
		else:
			# Load papers downloaded from Pubmed
			self.load_papers_pubmed()
			# Create vectorizer to generate matrix of word occurrences
			self.make_vectorizer()

	##------------------------------------------------------------------------##
	## make_partition_dataset function - Split into test and train partitions
	##------------------------------------------------------------------------##
	def make_partition_dataset(self,no_of_samples,sample):
		'''
		Split dataset into test and train partitions
		'''
		# Set number of partitions as number of abstracts available
		samples = len(self.abstracts)
		# Ensure number of samples passed does not exceed number of partitions
		if no_of_samples > samples:
			no_of_samples = samples
		# Leave one sample out
		sample = sample - 1
		# Define number of elements in partitions
		chunk_size = float(samples)/no_of_samples
		# Define start and end indexes of test partitions
		testing_chunk_start = int(chunk_size*sample)
		testing_chunk_end = int(chunk_size*sample + chunk_size)
		# Define object attributes as empty lists
		self.testing_abstracts = []
		self.testing_article_class = []
		self.training_abstracts = []
		self.training_article_class = []
		# Iterate over partitions
		for i in range(0,samples):
			# Check working within test partition
			if i >= testing_chunk_start and i < testing_chunk_end:
				# Add abstract and article class from partition as test object attributes
				self.testing_abstracts.append(self.abstracts[i])
				self.testing_article_class.append(self.article_classes[i])
			else:
				# Add abstract and article class outside partition as training object attributes
				self.training_abstracts.append(self.abstracts[i])
				self.training_article_class.append(self.article_classes[i])

	##------------------------------------------------------------------------##
	## make_benchmarking_dataset method - Save data as ocurrence matrix and classes
	##------------------------------------------------------------------------##
	def make_benchmarking_dataset(self):
		'''
		Save test and train data as ocurrence matrix and classes
		'''
		# Do unsupervised transformation of training abstracts into matrix of ocurrences and save as attribute
		self.transformed_training_abstracts = self.vectorizer.fit_transform(self.training_abstracts)
		# Copy training abstract class as attribute
		self.transformed_training_article_class = self.training_article_class
		# Do unsupervised transformation of testing abstracts into matrix of ocurrences and save as attribute
		self.transformed_testing_abstracts = self.vectorizer.transform(self.testing_abstracts)
		# Copy testing abstract class as attribute
		self.transformed_testing_article_class = self.testing_article_class
		
	##------------------------------------------------------------------------##
	## make_training_dataset method - Save data as ocurrence matrix and classes
	##------------------------------------------------------------------------##
	def make_training_dataset(self):
		'''
		Save data as ocurrence matrix and classes 
		'''
		# Do unsupervised transformation of abstracts into matrix of ocurrences with selected vectorizer, and save as attribute
		if self.options["vectorizer_type"] == "TfidfVectorizer":
			self.transformed_training_abstracts = self.vectorizer.fit_transform(self.abstracts)
		if self.options["vectorizer_type"] == "HashingVectorizer":
			self.transformed_training_abstracts = self.vectorizer.transform(self.abstracts)
		# Copy abstract class as attribute

		self.transformed_training_article_class = self.article_classes

	
	##########################################################################################
	##########################################################################################
	##########################################################################################
	#BENCHMARK BENCHMARK BENCHMARK BENCHMARK BENCHMARK BENCHMARK BENCHMARK BENCHMARK BENCHMARK
	##########################################################################################
	##########################################################################################
	##########################################################################################

	##------------------------------------------------------------------------##
	## benchmark method - Run benchmark and return output metrics
	##------------------------------------------------------------------------##
	def benchmark(self,clf):
		'''
		Run benchmark and return output metrics
		'''
		# Save initial time of fitting step
		t0 = time.time()
		clf.fit(self.transformed_training_abstracts, self.training_article_class)
		# Save running time of fitting step
		train_time = time.time() - t0
		# Save initial time of predicting step
		t0 = time.time()
		# Predict class for testing abstracts
		pred = clf.predict(self.transformed_testing_abstracts)
		# Save running time of predicting step
		testing_time = time.time() - t0
		# Define empty dictionary for metrics
		benchmark_metrics = {}
		# Store benchmark metrics as object attributes
		benchmark_metrics["train time"] =  train_time
		benchmark_metrics["test time"] =  testing_time
		benchmark_metrics['Accuracy'] = metrics.accuracy_score(self.testing_article_class, pred)
		benchmark_metrics['F1 score'] = metrics.f1_score(self.testing_article_class, pred, average='weighted')
		benchmark_metrics['Recall'] = metrics.recall_score(self.testing_article_class, pred, average='weighted')
		benchmark_metrics['Precision'] =  metrics.precision_score(self.testing_article_class, pred, average='weighted')
		# Return output metrics from benchmark
		return benchmark_metrics

	##------------------------------------------------------------------------##
	## run_benchmark method - Run benchmark for all selected methods
	##------------------------------------------------------------------------##
	def run_benchmark(self):
		'''
		Run benchmark for all selected methods and save as object attribute
		'''
		if "Ridge Classifier" in self.benchmark_methods: self.benchmark_results["Ridge Classifier"].append(self.benchmark(RidgeClassifier(tol=1e-2, solver="lsqr")))
		if "Perceptron" in self.benchmark_methods: self.benchmark_results["Perceptron"].append(self.benchmark(Perceptron(max_iter=50)))
		if "Passive-Aggressive" in self.benchmark_methods: self.benchmark_results["Passive-Aggressive"].append(self.benchmark(PassiveAggressiveClassifier(max_iter=50)))
		if "kNN" in self.benchmark_methods: self.benchmark_results["kNN"].append(self.benchmark(KNeighborsClassifier(n_neighbors=10)))
		if "Random forest" in self.benchmark_methods: self.benchmark_results["Random forest"].append(self.benchmark(RandomForestClassifier(n_estimators=100)))
		if "LinearSVC %s penalty l1" in self.benchmark_methods: self.benchmark_results["LinearSVC %s penalty l1"].append(self.benchmark(LinearSVC(loss='squared_hinge', penalty="l1", dual=False, tol=1e-3)))
		if "SGDClassifier %s penalty l1" in self.benchmark_methods: self.benchmark_results["SGDClassifier %s penalty l1"].append(self.benchmark(SGDClassifier(alpha=.0001, max_iter=50, penalty="l1")))
		if "LinearSVC %s penalty l2" in self.benchmark_methods: self.benchmark_results["LinearSVC %s penalty l2"].append(self.benchmark(LinearSVC(loss='squared_hinge', penalty="l2", dual=False, tol=1e-3)))
		if "SGDClassifier %s penalty l2" in self.benchmark_methods: self.benchmark_results["SGDClassifier %s penalty l2"] .append(self.benchmark(SGDClassifier(alpha=.0001, max_iter=50, penalty="l2")))
		if "RBF SVC %s penalty" in self.benchmark_methods: self.benchmark_results["RBF SVC %s penalty"].append(self.benchmark(SVC(kernel='rbf', gamma=0.7, C=1.0)))
		if "Elastic-Net penalty" in self.benchmark_methods: self.benchmark_results["Elastic-Net penalty"].append(self.benchmark(SGDClassifier(alpha=.0001, max_iter=50, penalty="elasticnet")))
		if "NearestCentroid (aka Rocchio classifier)" in self.benchmark_methods: self.benchmark_results["NearestCentroid (aka Rocchio classifier)"].append(self.benchmark(NearestCentroid()))
		if "MultinomialNB Naive Bayes" in self.benchmark_methods: self.benchmark_results["MultinomialNB Naive Bayes"].append(self.benchmark(MultinomialNB(alpha=.01)))
		if "BernoulliNB Naive Bayes" in self.benchmark_methods: self.benchmark_results["BernoulliNB Naive Bayes"].append(self.benchmark(BernoulliNB(alpha=.01)))
		if "LinearSVC with L1-based feature selection" in self.benchmark_methods: self.benchmark_results["LinearSVC with L1-based feature selection"].append(self.benchmark(Pipeline([ ('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False, tol=1e-3))), ('classification', LinearSVC())])))
		if "LinearSVC with L2-based feature selection" in self.benchmark_methods: self.benchmark_results["LinearSVC with L2-based feature selection"].append(self.benchmark(Pipeline([ ('feature_selection', SelectFromModel(LinearSVC(penalty="l2", dual=False, tol=1e-3))),  ('classification', LinearSVC())])))

	##------------------------------------------------------------------------##
	## print_benchmark method - Print benchmark results
	##------------------------------------------------------------------------##
	def print_benchmark(self):
		'''
		Print average metric scores for benchmark
		'''
		# Define and print headers
		benchmark_headers = ['train', 'test', 'Prec', 'Rec', 'F1', 'Acc']
		print '\t'.join(benchmark_headers + ["method"])
		# Iterate over results from benchmark methods
		for method in self.benchmark_results:
			# Define empty dict for metric scores
			benchmark_metric_score = {}
			# Iterate over list of metrics
			for benchmark_metric in ['train time', 'test time', 'Precision', 'Recall', 'F1 score', 'Accuracy']:
				# Define empty list as values in metric scores dict (key: metric)
				benchmark_metric_score[benchmark_metric] = []
			# Iterate over partitions in result from current method
			for sample in self.benchmark_results[method]:
				# Iterate over list of metrics
				for benchmark_metric in ['train time', 'test time', 'Precision', 'Recall', 'F1 score', 'Accuracy']:
					# Append metric scores for current partition to value list in metric scores dict (key: metric)
					try:
						benchmark_metric_score[benchmark_metric].append(sample[benchmark_metric])
					except:
						pass
			# Define empty list of average metric scores
			benchmark_metrics = []
			# Iterate over list of metrics
			for benchmark_metric in ['train time', 'test time', 'Precision', 'Recall', 'F1 score', 'Accuracy']:
				# Append average metric score for benchmark method to list of average metric scores
				benchmark_metrics.append("%0.3f"%(float(sum(benchmark_metric_score[benchmark_metric]))/len(benchmark_metric_score[benchmark_metric])))
			# Append benchmark method name to list of average metric scores
			benchmark_metrics += [method]
			# Print average metric scores
			print '\t'.join(benchmark_metrics)
	
	##########################################################################################
	##########################################################################################
	##########################################################################################
	#TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN TRAIN
	##########################################################################################
	##########################################################################################
	##########################################################################################

	##------------------------------------------------------------------------##
	## train_classifier method - Train classifier
	##------------------------------------------------------------------------##	
	def train_classifier(self):
		'''
		Train classifier
		'''
		model_data = {}
		# Set matrix of word occurrences and feature names in dict
		model_data["vectorizer"] = self.vectorizer
		model_data["feature_names"] = self.vectorizer.get_feature_names()
		# Set chosen method and its parameters as model
		if self.options["classifier_method"] == "Elastic-Net penalty":
			model_data["model"]  = SGDClassifier(alpha=.0001, max_iter=50, penalty="elasticnet")
		elif self.options["classifier_method"] == "RidgeClassifier":
			model_data["model"]  = RidgeClassifier(tol=1e-2, solver="lsqr")
		elif self.options["classifier_method"] == "PassiveAggressiveClassifier":
			model_data["model"]  = (PassiveAggressiveClassifier(max_iter=50), "Passive-Aggressive"),
		elif self.options["classifier_method"] == "LinearSVC_L1":
			model_data["model"]  = LinearSVC(loss='squared_hinge', penalty="l1", dual=False, tol=1e-3)
		elif self.options["classifier_method"] == "LinearSVC_L2":
			model_data["model"]  = LinearSVC(loss='squared_hinge', penalty="l2", dual=False, tol=1e-3)
		elif self.options["classifier_method"] == "SGDClassifier_L1":
			model_data["model"]  =  SGDClassifier(alpha=.0001, max_iter=50, penalty="l1")
		elif self.options["classifier_method"] == "SGDClassifier_L2":
			model_data["model"]  =  SGDClassifier(alpha=.0001, max_iter=50, penalty="l2")
		elif self.options["classifier_method"] == "LinearSVC_L1_feature":
			model_data["model"]  = benchmark(Pipeline([('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False, tol=1e-3))),('classification', LinearSVC())]))
		elif self.options["classifier_method"] == "LinearSVC_L2_feature":
			model_data["model"]  = benchmark(Pipeline([('feature_selection', SelectFromModel(LinearSVC(penalty="l2", dual=False, tol=1e-3))),('classification', LinearSVC())]))
		else:
			model_data["model"]  = LinearSVC(loss='squared_hinge', penalty="l2", dual=False, tol=1e-3)
		# Fit abstract ocurrence matrix and classes in training dataset to model
		print "Fitting models using",len(self.abstracts), "abstracts and",len(set(self.transformed_training_article_class )), "classes"
		model_data["model"].fit(self.transformed_training_abstracts, self.transformed_training_article_class)
		# Calculate p-value distribution
		print "Calculating p-value distribution using",len(self.article_data), "abstracts.",
		prob_dist = {}
		cum_prob_dist = {}
		summer = 0
		count = 0
		# Iterate over PMIDs
		for pmid in self.article_data:
		  # Join words and link to PMID as object attribute
			count += 1
			abstract = [self.article_data[pmid]["training_text"] ]
			scores = {}
			vectorized_abstract = model_data["vectorizer"].transform(abstract)
			# Classify abstract based on train model
			prob = model_data["model"].predict(vectorized_abstract)  # predicted class labels
			probs = model_data["model"].decision_function(vectorized_abstract)  # predicted class probabilities
			# Iterate over class probabilities
			for i in range(0,len(probs[0])):
				# Get score and labels for each class
				score = probs[0][i]
				prediction_class = model_data["model"].classes_[i]
				# Skip if label already applied to article
				if prediction_class in self.article_data[pmid]['class']: continue
				# Initialize or increment probability counts
				if float("%1.3f"%score) not in prob_dist:
					prob_dist[float("%1.3f"%score)] = 0
				prob_dist[float("%1.3f"%score)] += 1
		print "Calculated."
		# Calculate cumulative probability
		sorted_prob_dist = prob_dist.keys()
		sorted_prob_dist.sort()  # sort probability counts
		denom = sum(prob_dist.values())  # sum probability counts
		max_score = max(sorted_prob_dist)  # define max probability boundary
		min_score = min(sorted_prob_dist)  # define min probability boundary
		last = 0
		for score in range(int(min_score*1000)-1,int(max_score*1000) + 1):  # use 1000 bins
			score = float(score)/1000  # place score in bin
			if score in prob_dist:
				summer += prob_dist[score]  # increase bin frequency by one
				cum_prob_dist[score] = float(summer)/denom  # standardize counts of bin
				last = float(summer)/denom  # cumulative probability of bin
			else:
				cum_prob_dist[score] = last
		# Copy cumulative probability as model attribute
		model_data["probability"] = cum_prob_dist

		# Copy number of training abstract classes as model attribute	  
		training_data = {"samples":{}}
		for classifier_class in model_data["model"].classes_:
			training_data["samples"][classifier_class] = self.transformed_training_article_class.count(classifier_class)
		model_data["training_data"] = training_data
		# Copy PMIDs as model attribute
		model_data["pmids"] = self.article_data.keys()

		# Save model to pickle file
		print "Saving model"
		open(self.options["model_path"] ,"w").write(pickle.dumps(model_data))


	##########################################################################################
	##########################################################################################
	##########################################################################################
	#CLASSIFY CLASSIFY CLASSIFY CLASSIFY CLASSIFY CLASSIFY CLASSIFY CLASSIFY CLASSIFY CLASSIFY
	##########################################################################################
	##########################################################################################
	##########################################################################################
	
	##------------------------------------------------------------------------##
	## run_classifier method - Run classifier
	##------------------------------------------------------------------------##	
	def run_classifier(self):
		'''
		Get article, parse contents, run classifier and return scores
		'''
		self.publication_mapping = {}
		# Load available model
		if self.model_data == {}:
			self.model_data = pickle.loads(open(self.options["model_path"]).read())
			# Get counts in lower bin of cumulative probability
			sorted_probability = self.model_data["probability"].keys()
			sorted_probability.sort()  # sort bins of cumulative probability
			last = 0
			for i in range(int(sorted_probability[-1]*100),int(sorted_probability[0]*100) - 1,-1):  # use 100 bins
				if float("%1.2f"%(float(i)/100)) in self.model_data["probability"]:  # save cumulative probability per bin
					last = self.model_data["probability"][float("%1.2f"%(float(i)/100))]
				else:
					self.model_data["probability"][float(i)/100]  = last
			# Retrieve keywords from best predictions
			self.model_data["keywords"] = self.get_keywords()
		
		# Link title, abstract, annotations and MeSH terms into string
		abstract_text = self.options['title'] + " " + self.options["abstract"].replace('\n', ' ') 
		if self.options["classifier_use_protein_annotation"]:
			abstract_text +=  " ".join(self.options["pdb_annotation_tags"])
			abstract_text +=  " ".join(self.options["scilite_tags"])
		if self.options["classifier_use_mesh_terms"]:
			abstract_text +=  " ".join(self.options["mesh_terms"])
		transform_abstract = abstract_text
		
		# Download Punkt Tokenizer Model to parse english if needed
		self.nltk_download('tokenizers','punkt')
		# Split training text into words, spliting off punctuation other than periods
		tokens_unique = nltk.word_tokenize(transform_abstract.lower())
		# Convert splitted text into nltk.Text object
		tokens_text = nltk.Text(tokens_unique)
		# Download Averaged Perceptron Tagger if needed
		self.nltk_download('taggers','averaged_perceptron_tagger')
		# Classify words into lexical categories (POS-tagging) as tuples
		tagged = nltk.pos_tag(tokens_unique)
		
		# Create empty list of nouns
		nouns = []
		# Iterate over tagged words tuples
		for token in tagged:
			# todo: strip numbers away
			token_text = token[0]  # word
			token_type = token[1]  # tag
			# Avoid non-nouns if requested
			if self.options["strip_non_nouns"]:
				# Add nouns to nouns list
				if token_type in ["NN","NNS","NNP","NNPS"]:  # (single, plural) common, proper nouns
					nouns.append(token_text.lower().strip())
			# Add all words to nouns list
			else:
				nouns.append(token_text.lower().strip())
		# Join nouns from abstract for processing
		transform_abstract = [" ".join(nouns).replace("(","").replace(")","")]
		# Vectorize abstract
		vectorized_abstract = self.model_data["vectorizer"].transform(transform_abstract)
		# Return decision function values for model (distances to classifier hyperplanes)
		distances = self.model_data["model"].decision_function(vectorized_abstract)
		scores = {}
		ranker = {}
		significance_score = {}
		distance_score= {}
		top = [-1,None]
		# Iterate over distances
		for distance in distances[0]:
			# Get abstract labels 
			specificity_class_ =  self.model_data["model"].classes_[list(distances[0]).index(distance)]
			# Define abstract probability based on difference between distance and cumulative probability
			if distance > max(self.model_data["probability"]):
				probability = 0
			elif distance < min(self.model_data["probability"]):
				probability = 1
			elif float("%1.2f"%distance) not in self.model_data["probability"]:
				probability = 0
			else:
				probability = 1 - self.model_data["probability"][float("%1.2f"%distance)]
			# Apply Bonferroni correction
			bonferroni_corrected_probability = probability*len(self.model_data["model"].classes_)
			# Copy scores for labels as scores dict
			scores[specificity_class_] = {"distance":distance,"probability":probability,"bonferroni_corrected_probability":bonferroni_corrected_probability}
			# Check probability or distance exceed cutoffs
			keyword_matches = {}
			if probability < float(self.options['significance_cutoff']) or distance > float(self.options['distance_cutoff']):
				# Iterate over abstract words (nouns)
				for word in transform_abstract[0].split():
					# Keep abstract words marked as keywords from best predictions
					if word in self.model_data["keywords"][specificity_class_]:
						keyword_matches[word] = float(self.model_data["keywords"][specificity_class_][word])
				# Keep Bonferroni-corrected significant words
				if bonferroni_corrected_probability < float(self.options['significance_cutoff']):
					significance_score[specificity_class_] = keyword_matches
				# Keep distance-based significant words
				if distance > float(self.options['distance_cutoff']):
					if distance not in distance_score:
						distance_score = {}
					distance_score[specificity_class_] = keyword_matches
			# Keep labels and distance of top prediction		
			if distance > top[0]:
				top = [distance,specificity_class_]
		# Keep labels and words of top hit
		top_keyword = ""
		keyword_matches = {}
		for word in transform_abstract[0].split():
			if word in self.model_data["keywords"][top[1]]:
				keyword_matches[word] = float(self.model_data["keywords"][top[1]][word])
		top_hit = {top[1]:keyword_matches}
		# Store and return dict of PMID with title, scores, top labels and words, and training dataset membership
		self.publication_mapping[self.options["pmid"]] = {
		"title":self.options["title"],
		"scores":scores,
		"significant":significance_score,
		"distance":distance_score,
		"top":top_hit,
		"training_article":self.options["pmid"] in self.model_data["pmids"]
		}
		return self.publication_mapping

	##------------------------------------------------------------------------##
	## make_output_tdt method - Format article information as TDT
	##------------------------------------------------------------------------##	
	def make_output_tdt(self):
		'''
		Format article information as TDT
		'''
		for pmid in self.publication_mapping:
			# Retrieve selected labels in PMID by selected type of classification score
			if self.options['classification_type'] == "top":
				hits = self.publication_mapping[pmid]["top"]			
			if self.options['classification_type'] == "significant":
				hits = self.publication_mapping[pmid]["significant"]
			if self.options['classification_type'] == "distance":
				hits = self.publication_mapping[pmid]["distance"]
			# Iterate over labels and extract information
			for specificity_class_ in hits:
				scores = self.publication_mapping[pmid]["scores"][specificity_class_]
				title = self.publication_mapping[pmid]["title"]
				authors = self.publication_mapping[pmid]["title"]
				training_article = self.publication_mapping[pmid]["training_article"]				
				distance = scores["distance"]
				probability = scores["bonferroni_corrected_probability"]				
				# Sort a copy of the PMID labels dictionary
				sorted_keyword_matches = sorted(hits[specificity_class_].items(), key=operator.itemgetter(1))
				# Get five top labels
				top_keyword = []
				if len(sorted_keyword_matches) > 0:
					for i in range(0,5):
						try:
							top_keyword.append(sorted_keyword_matches[-1 - i][0] + ":" + "%1.2f"%sorted_keyword_matches[-1 - i][1])
						except:
							pass
					top_keyword = ";".join(top_keyword)
				# Make output string with PMID scores and brief info, if requested
				if self.options['show_training_articles']:
					self.output_str += pmid + "\t" + str(training_article) + "\t" +"%1.2g"%(probability) + "\t" + "%1.3f"%distance + "\t" + top_keyword  + "\t" + str(specificity_class_)  + "\t" + title + "\n"
				else:
					if training_article == False:
						self.output_str += pmid + "\t" + str(training_article) + "\t" + "%1.2g"%(probability) + "\t" + "%1.3f"%distance + "\t" + + top_keyword + "\t" + str(specificity_class_)  + "\t" + title + "\n"

	##------------------------------------------------------------------------##
	## show_keywords method - Show best labels
	##------------------------------------------------------------------------##	
	def show_keywords(self):
		'''
		Show top matched words (labels)
		'''
		counter = 0
		self.publication_mapping = {}
		# Load available model
		model_data = pickle.loads(open(self.options["model_path"]).read())
		# Get feature names and coefficients for top 10 models
		for i, category in enumerate(model_data["model"].classes_):
			top10 = np.argsort(model_data["model"].coef_[i])[-10:]		
			print category,"\t","\t",model_data["training_data"]["samples"][category],"\t",
			best_matches = [ model_data["feature_names"][x] + ":" + "%1.2f"%(model_data["model"].coef_[i][x]) for x in top10]
			best_matches.reverse()
			print "\t".join(best_matches)

	##------------------------------------------------------------------------##
	## get_keywords method - Retrieve top keywords
	##------------------------------------------------------------------------##	
	def get_keywords(self):
		'''
		Retrieves keywords from 100 best predictions
		'''
		counter = 0
		self.publication_mapping = {}
		# Load available model
		model_data = pickle.loads(open(self.options["model_path"]).read())
		keywords = {}
		# Iterate for annotated classes
		for i, category in enumerate(model_data["model"].classes_):
			# Get indexes from best 100 predictions in model
			top100 = np.argsort(model_data["model"].coef_[i])[-100:]
			# Get best 100 predictions
			best_matches = {}
			for x in top100:
				best_matches[model_data["feature_names"][x]] = model_data["model"].coef_[i][x]
			# Save keywords from 100 best predictions
			if category.split(":")[0] != -1:
				keywords[category.split(":")[0]] = best_matches
		# Return best 100 keywords
		return keywords
		
	##------------------------------------------------------------------------##
	## run_benchmarking function - Wrapper to run benchmark
	##------------------------------------------------------------------------##		
	def run_benchmarking(self):
		'''
		Wrapper to configure, run and print benchmark
		'''
		# Load and setup data
		self.setup_data()
#		return()  # TEMP EXIT
		# Define as many partitions as articles available if requested (xfold defined in config.ini)
		if self.options["xfold"] == "all":
			self.options["xfold"] = len(self.abstracts) - 1
		# Iterate over all partitions
		for sample_iter in range(1,int(self.options["xfold"]) + 1):
			try:
				print str(self.options["xfold"]) + "-cross validation sample ",sample_iter
				# Make test and train partitions
				self.make_partition_dataset(int(self.options["xfold"]),sample_iter)
				# Save test and train data as ocurrence matrix and classes
				self.make_benchmarking_dataset()
				# Run benchmark for all selected methods
				self.run_benchmark()
			except:
				raise
		# Print average metric scores for all methods in benchmark
		self.print_benchmark()
	
	##------------------------------------------------------------------------##
	## train_abstract_classifier method - Wrapper to train classifier
	##------------------------------------------------------------------------##
	def train_abstract_classifier(self):
		'''
		Wrapper to configure and train classifier
		'''
		# Load and setup data
		self.setup_data()
		# Save train data as ocurrence matrix and classes 
		self.make_training_dataset()
		# Train classifier
		self.train_classifier()
	
	##------------------------------------------------------------------------##
	## classify_article method - Wrapper to classify an article
	##------------------------------------------------------------------------##
	def classify_article(self,pmid=""):
		'''
		Wrapper to classify an article
		'''
		self.output_str = ""
		self.classifications = {}
		# Check and read existing PMID files
		if self.options["pmid_file"] != None and self.options["pmid_file"] != "":
			if os.path.exists(self.options["pmid_file"]):
				pmids = open(self.options["pmid_file"]).read().strip().split("\n")
			else:
				print self.options["pmid_file"] ,"does not exist"
		else:
			if pmid != "":
				self.options["pmid"] = str(pmid)
			pmids = str(self.options["pmid"]).split(",")
		# Iterate over PMIDs
		for pmid in pmids:
			# Retrieve and parse articles
			response = self.getReference(pmid)
			if response[u'status'] == u'Error':
				print "Error retrieving article:",pmid
				continue
			# Copy article data as object attributes
			self.options["pmid"] = pmid
			self.options["title"] = response["data"][u'title'] 
			self.options["abstract"] = response["data"]["abstract_text"]
			self.options["mesh_terms"] = response["data"]["mesh_terms"]
			self.options["pdb_annotation_tags"] = []
			self.options["scilite_tags"] = []
			if self.options["classifier_use_protein_annotation"]:
				if "pdb_annotation_tags" in response:
					self.options["pdb_annotation_tags"]  = response["pdb_annotation_tags"]
				if "scilite_tags" in response:
					self.options["scilite_tags"] =  response["scilite_tags"]
			# Classify article and add to existing classifications
			classification = self.run_classifier()
			self.classifications.update(classification)
			# Make article data into TDT format, if requested
			if self.options["show_output"] or (self.options["make_output"] and self.options["output_type"] == 'tdt'):
				self.make_output_tdt()
		# Print PMID scores and brief info, if requested
		if self.options["show_output"]:
			if self.options["output_type"] == 'tdt':
				print self.output_str.strip()
			else:
				pprint.pprint(self.classifications)
		# Save article data as TDT or JSON file
		if self.options["make_output"]:
			if self.options["output_type"] == 'tdt':
				print "Saving TDT-formatted file:",self.options["output_path"]
				open(self.options["output_path"],"w").write(self.output_str)
			elif self.options["output_type"] == 'json':
				print "Saving JSON-formatted file:",self.options["output_path"]
				with open(self.options["output_path"], 'w') as outfile:
					json.dump(self.classifications, outfile)

	##------------------------------------------------------------------------##
	## classify_article method - Wrapper to classify all Pubmed
	##------------------------------------------------------------------------##
	def classify_pubmed(self):
		'''
		Wrapper to classify all Pubmed records at once
		'''
		# Retrieve Pubmed records
		self.grabPubmedXml()
		# Classify Pubmed records
		self.classifyPubmed()
	
	##------------------------------------------------------------------------##
	## list_available_classifiers method - list available trained models
	##------------------------------------------------------------------------##
	def list_available_classifiers(self):
		'''
		List already trained models for machine learning-based classification
		'''
		self.classifier_lists = {}
		# Iterate over models dict
		for file in os.listdir(self.options["models_path"]):
			file_bits = file.split(".")
			# Add model pickle to dict if available
			if file_bits[-1] == 'pickle':
				if file_bits[0] not in self.classifier_lists:
					self.classifier_lists[file_bits[0]] = []
				self.classifier_lists[file_bits[0]].append(file_bits[1])
				
	##------------------------------------------------------------------------##
	## setup_job method - setup classifier options and paths
	##------------------------------------------------------------------------##
	def setup_job(self):
		'''
		Setup classifier options and paths for benchmark and/or training
		'''
		# List available trained models
		self.list_available_classifiers()
		# Show available trained models
		if self.options["show_classifiers"]:
			if len(self.classifier_lists) == 0:
				print "No classifiers available"
			else:
				print "# Classifiers available: " + str(len(self.classifier_lists))
				print "  Classifier\tMethod(s)"
				for classifier in self.classifier_lists:
					print classifier,"\t","[" + "|".join(self.classifier_lists[classifier]) + "]"
		# Benchmarking - skip setup
		elif self.options["run_benchmarking"]:
			pass
		# Classify all Pubmed records
		elif self.options["classify_pubmed"]:
			while True:
				try:
					choice = raw_input("This is a really bad idea and will download several gigabytess of data. Are you sure you want to continue (y|n)\n> ")
					if choice == 'y' :
						print "I warned you"
						break
					if choice == 'n' :
						print "Exiting"
						sys.exit()
				except:
					raise
		# Training - check options are set and paths exist
		elif self.options["train_abstract_classifier"]:
			if self.options["classifier_name"] == '':
				print "--classifier_name: option is not set"
			if self.options["training_data_type"] not in ["json","tdt"]:
				print "--training_data_type: option must be [json|tdt]"
				sys.exit()
			if not os.path.exists(self.options["training_data"]):
				print "--training_data:  path - " + self.options["training_data"] + " -  does not exist"
				sys.exit()
		# Classification - check options are set and paths exist
		else:
			# Check for type of classification
			if self.options['classification_type'] not in ["top","significant","distance"]:
				print "--classification_type: option must be [top|significant|distance]"
				sys.exit()
			# Check for available models
			if self.options["classifier_name"] == '':
				print "--classifier_name: option is not set"
				if len(self.classifier_lists) == 0:
					print "No classifiers available"
				else:
					print "# Classifiers available: " + str(len(self.classifier_lists))
					print "  Classifier\tMethod(s)"
					for classifier in self.classifier_lists:
						print classifier,"\t","[" + "|".join(self.classifier_lists[classifier]) + "]"
				sys.exit()
			# Check for output options
			if self.options["make_output"]:
				if self.options["output_path"] == "":
					print "--output_path: option is not set"
					sys.exit()
				if not os.path.exists(os.path.dirname(self.options["output_path"])):
					print "--output_path: containing directory does not exist"
					sys.exit()
				if self.options["output_type"] not in ['tdt','json']:
					print "--output_type: option '" + self.options["output_type"] + "' not found [json|tdt]"
					sys.exit()
			# Check PMID is passed and file exists if classifying abstracts
			if self.options["classify_abstract"] and (self.options["pmid"] == None and self.options["pmid_file"] == None):
				print "--classify_abstract requires a --pmid option"
				sys.exit()
			if self.options["pmid_file"] != None and self.options["pmid"] == None :
				if not os.path.exists(self.options["pmid_file"]):
					print "--pmid_file: " + self.options["pmid_file"] + " does not exist"
		# Benchmarking		
		if self.options["run_benchmarking"]:
			if self.options["classifier_method"] not in self.benchmark_methods_options:  # Check model is one of the allowed
				print "--classifier_name: ", self.options["classifier_method"], "not available"
				for benchmark_methods_option in self.benchmark_methods_options:
					print "# Classifier Option - ",self.benchmark_methods_options[benchmark_methods_option]
					print "  Usage: --classifier_name " + benchmark_methods_option
				sys.exit()
			# Define model and path options
			self.options["classifier_method"] = self.benchmark_methods_options[self.options["classifier_method"]]
			self.options["model_path"] =  os.path.abspath(os.path.join(self.options["models_path"], self.options["classifier_name"] + "." + self.options["classifier_method"].replace(" ","_").lower() + ".model.pickle"))
			# Skip if training on abstracts
			if self.options["train_abstract_classifier"] or self.options["run_benchmarking"]:
				pass
			# Check model trained on abstracts exists
			else:
				if not os.path.exists(self.options["model_path"]):
					print "# Abstract classifier " + self.options["classifier_name"] + "." + self.options["classifier_method"].replace(" ","_").lower() + " cannot be found"
					print "  Please run --train_abstract_classifier"
					sys.exit()
			
	##------------------------------------------------------------------------##
	## run_job method - Run classifier job
	##------------------------------------------------------------------------##
	'''
	Run job for benchmark, training and/or classification
	'''
	def run_job(self):
		#Setup job
		self.setup_paths()
		self.setup_job()
		
		# Run benchmark
		if self.options["run_benchmarking"]:
			print "Running benchmark"
			self.run_benchmarking()	
		# Run training
		if self.options["train_abstract_classifier"]:
			self.train_abstract_classifier()
		# Classify abstract
		if self.options["classify_abstract"]:
			self.classify_article()
		# Classify candidates
		if self.options["classify_candidates"]:
			self.classify_candidates()
		# Show relevant keywords
		if self.options["show_keywords"]:
			 self.show_keywords()
		# Classify Pubmed abstract
		if self.options["classify_pubmed"]:
			 self.classify_pubmed()

################################################################################
## END OF SECTION II                                                          ##
################################################################################


################################################################################
## SECTION III: MAIN PROGRAM                                                  ##
################################################################################

if __name__ == "__main__":

	# Create object of class OptionParser 
	op = optparse.OptionParser()  # DEPRECATED (v2.7+)
	# Create object of class articleClassifier
	classifier = articleClassifier()

	# Overwrite options from commandline arguments
	classifier.options.update(option_reader.load_commandline_options(classifier.options,classifier.options_help))
	# Run classifier
	classifier.run_job()
	
################################################################################
## END OF SECTION III                                                         ##
################################################################################
