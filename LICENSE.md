Annotation Helper  - Classify abstracts from scientific publications 
to help in annotation.  
Copyright (C) 2018-2019 Norman E. Davey, Nicolas Palopoli 
for the [IDPfun](http://idpfun.eu) project.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Author contact:  
Norman E. Davey. 
Division of Cancer Biology, The Institute of Cancer Research, London, UK. 
<normandavey@gmail.com>  
Nicolas Palopoli. 
Universidad Nacional de Quilmes - CONICET, Buenos Aires, Argentina. 
<nicopalo@gmail.com>  